package br.com.martinho.githubrepositories.listRepositoriesTest;

import android.content.ComponentName;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import java.util.ArrayList;
import java.util.List;

import br.com.martinho.githubrepositories.BuildConfig;
import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.presentation.listPullRequests.ListPullRequestsActivity;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesActivity;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesAdapter;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesContract;

import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created by Thiago on 25/09/2016.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 23)
public class ListRepositoriesActivityTest {

    private ListRepositoriesActivity activity;

    @Mock
    ListRepositoriesContract.Presenter presenter;

    @Before
    public void setUp() {
        this.activity = Robolectric.setupActivity(ListRepositoriesActivity.class);
    }

    @Test
    public void shouldFindAllViews() throws Exception {
        View recyclerView = this.activity.findViewById(R.id.activity_list_repositories_list);
        assertNotNull("RecyclerView could not be found", recyclerView);
        assertTrue(recyclerView instanceof RecyclerView);

        View progressBar = this.activity.findViewById(R.id.activity_list_repositories_progress_bar);
        assertNotNull("Progress Bar could not be found", progressBar);
        assertTrue(progressBar instanceof ProgressBar);
    }

    @Test
    public void shouldExistsSupportActionBar() throws Exception {
        assertNotNull(this.activity.getSupportActionBar());
    }

    @Test
    public void showLoadingLayout() throws Exception {
        this.activity.showLoadingLayout();
        assertTrue(this.activity.findViewById(R.id.activity_list_repositories_progress_bar).getVisibility() == View.VISIBLE);
    }

    @Test
    public void addData() throws Exception {
        List<Repository> repositories = new ArrayList<>();
        RecyclerView recyclerView = (RecyclerView) this.activity.findViewById(R.id.activity_list_repositories_list);
        ListRepositoriesAdapter adapter = new ListRepositoriesAdapter(this.activity, repositories);
        recyclerView.setAdapter(adapter);
        repositories.add(new Repository());
        this.activity.addData(repositories);

        assertTrue(recyclerView.getAdapter().getItemCount() > 0);
        assertTrue(this.activity.findViewById(R.id.activity_list_repositories_progress_bar).getVisibility() == View.GONE);

    }

    @Test
    public void showErrorLayout() throws Exception {
        this.activity.showErrorLayout(1);
        assertTrue(this.activity.findViewById(R.id.activity_list_repositories_progress_bar).getVisibility() == View.GONE);
    }

    @Test
    public void onClickItem() throws Exception {
        this.activity.onClickItem("", "");

        ShadowActivity shadowActivity = Shadows.shadowOf(this.activity);
        assertThat(shadowActivity.peekNextStartedActivityForResult().intent.getComponent(), equalTo(new ComponentName(this.activity, ListPullRequestsActivity.class)));
    }
}
