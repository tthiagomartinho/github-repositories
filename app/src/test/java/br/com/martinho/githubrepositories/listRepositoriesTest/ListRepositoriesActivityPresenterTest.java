package br.com.martinho.githubrepositories.listRepositoriesTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import br.com.martinho.githubrepositories.data.repository.GitHubRepositoriesRepository;
import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesActivityPresenter;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesContract;
import rx.Observable;

import static org.mockito.Mockito.when;


/**
 * Created by Thiago on 25/09/2016.
 */

public class ListRepositoriesActivityPresenterTest {

    @Mock
    GitHubRepositoriesRepository repository;

    @Mock
    ListRepositoriesContract.View view;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ListRepositoriesActivityPresenter presenter = new ListRepositoriesActivityPresenter(repository);
        presenter.setView(view);
    }

    @Test
    public void shouldReturnResponseList() throws Exception {
        List<Repository> repositories = new ArrayList<>();
        when(repository.getRepositories(1)).thenReturn(Observable.just(repositories));
    }
}
