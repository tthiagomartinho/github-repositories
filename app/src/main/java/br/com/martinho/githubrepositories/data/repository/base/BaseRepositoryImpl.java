package br.com.martinho.githubrepositories.data.repository.base;

import okhttp3.Headers;

import static br.com.martinho.githubrepositories.util.Constants.LAST;
import static br.com.martinho.githubrepositories.util.Constants.LINK_HEADER;
import static br.com.martinho.githubrepositories.util.Constants.PAGE_EQUALS;
import static br.com.martinho.githubrepositories.util.Constants.REL_LINK_HEADER;

/**
 * Created by Thiago on 02/10/2016.
 */

public abstract class BaseRepositoryImpl implements BaseRepository {

    protected int maxPages = -1;

    @Override
    public int parseHeader(Headers headers) {
        String linkHeader = headers.get(LINK_HEADER);
        if (linkHeader != null) {
            String[] links = linkHeader.split(",");
            for (String link : links) {
                String[] segments = link.split(";");
                if (segments.length < 2)
                    continue;

                String linkPart = segments[0].trim();
                if (!linkPart.startsWith("<") || !linkPart.endsWith(">")) //$NON-NLS-1$ //$NON-NLS-2$
                    continue;
                linkPart = linkPart.substring(1, linkPart.length() - 1);

                for (int i = 1; i < segments.length; i++) {
                    String[] rel = segments[i].trim().split("="); //$NON-NLS-1$
                    if (rel.length < 2 || !REL_LINK_HEADER.equals(rel[0]))
                        continue;

                    String relValue = rel[1];
                    if (relValue.startsWith("\"") && relValue.endsWith("\"")) //$NON-NLS-1$ //$NON-NLS-2$
                        relValue = relValue.substring(1, relValue.length() - 1);

                    if (LAST.equals(relValue) && linkPart.lastIndexOf(PAGE_EQUALS) > -1) {
                        return Integer.parseInt(linkPart.substring(linkPart.lastIndexOf(PAGE_EQUALS) + 5));
                    }
                }
            }
        }
        return -1;
    }

    @Override
    public boolean canRequestMoreData(int page) {
        return page <= this.maxPages || this.maxPages == -1;
    }
}
