package br.com.martinho.githubrepositories.presentation.base;

/**
 * Created by Thiago on 21/09/2016.
 */

public interface BasePresenterContract {

    void onDestroy();
}
