package br.com.martinho.githubrepositories.data.repository.base;

import okhttp3.Headers;

/**
 * Created by Thiago on 02/10/2016.
 */

public interface BaseRepository {

    boolean canRequestMoreData(int page);

    int parseHeader(Headers headers);

}
