package br.com.martinho.githubrepositories.presentation.listRepositories;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.domain.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Thiago on 06/10/2016.
 */

public class ListRepositoriesAdapter extends RecyclerView.Adapter<ListRepositoriesAdapter.ViewHolder> {
    private List<Repository> repositories;

    ListRepositoriesContract.View view;
    private Context context;

    public ListRepositoriesAdapter(ListRepositoriesContract.View view, List<Repository> repositories) {
        this.view = view;
        this.repositories = repositories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.adapter_repositories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = this.repositories.get(position);

        if (!TextUtils.isEmpty(repository.getOwner().getAvatarUrl())) {
            Picasso.with(this.context).load(repository.getOwner().getAvatarUrl()).into(holder.image);
        }

        holder.name.setText(repository.getName());
        holder.username.setText(repository.getOwner().getLogin());
        holder.userFullName.setText(repository.getFullName());
        holder.description.setText(repository.getDescription());
        holder.forks.setText(repository.getForksCount());
        holder.stars.setText(repository.getStargazersCount());

        holder.view.setOnClickListener(v -> this.view.onClickItem(repository.getOwner().getLogin(), repository.getName()));

    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    void addMoreItems(List<Repository> repositories) {
        this.repositories.addAll(repositories);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.adapter_repositories_name)
        TextView name;
        @BindView(R.id.adapter_repositories_image)
        ImageView image;
        @BindView(R.id.adapter_repositories_username)
        TextView username;
        @BindView(R.id.adapter_repositories_user_full_name)
        TextView userFullName;
        @BindView(R.id.adapter_repositories_description)
        TextView description;
        @BindView(R.id.adapter_repositories_forks)
        TextView forks;
        @BindView(R.id.adapter_repositories_stars)
        TextView stars;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
        }
    }
}
