package br.com.martinho.githubrepositories.presentation.listPullRequests;

import java.util.List;

import br.com.martinho.githubrepositories.domain.PullRequest;
import br.com.martinho.githubrepositories.presentation.base.BasePresenterContract;
import br.com.martinho.githubrepositories.presentation.base.BaseViewContract;

/**
 * Created by Thiago on 07/10/2016.
 */

public interface ListPullRequestsContract {

    interface View extends BaseViewContract {

        void addData(List<PullRequest> repositories);

        void showErrorLayout();

        void onItemClick(String url);
    }

    interface Presenter extends BasePresenterContract {

        void setView(ListPullRequestsContract.View pullRequestsActivity);

        void loadData(String owner, String repository);
    }
}
