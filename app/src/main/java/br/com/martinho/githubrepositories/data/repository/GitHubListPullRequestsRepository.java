package br.com.martinho.githubrepositories.data.repository;

import java.util.List;

import br.com.martinho.githubrepositories.data.repository.base.BaseRepository;
import br.com.martinho.githubrepositories.domain.PullRequest;
import rx.Observable;

/**
 * Created by Thiago on 07/10/2016.
 */

public interface GitHubListPullRequestsRepository extends BaseRepository {

    Observable<List<PullRequest>> getPullRequests(String owner, String repository);
}
