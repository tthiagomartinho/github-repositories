package br.com.martinho.githubrepositories.domain;

import java.io.Serializable;

/**
 * Created by Thiago on 06/10/2016.
 */

public class Repository implements Serializable {

    private String name;

    private String fullName;

    private String description;

    private String forksCount;

    private String stargazersCount;

    private User owner;

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public String getForksCount() {
        return forksCount;
    }

    public String getStargazersCount() {
        return stargazersCount;
    }

    public User getOwner() {
        return owner;
    }
}
