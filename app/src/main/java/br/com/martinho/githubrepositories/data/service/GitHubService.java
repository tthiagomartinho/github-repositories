package br.com.martinho.githubrepositories.data.service;

import java.util.List;

import br.com.martinho.githubrepositories.domain.PullRequest;
import br.com.martinho.githubrepositories.domain.RepositoryResponse;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Thiago on 21/09/2016.
 */

public interface GitHubService {
    @GET("search/repositories")
    Observable<Response<RepositoryResponse>> getRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

    @GET("repos/{owner}/{repository}/pulls")
    Observable<List<PullRequest>> getPullRequests(@Path("owner") String owner, @Path("repository") String repository);
}
