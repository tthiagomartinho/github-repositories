package br.com.martinho.githubrepositories.data.repository.impl;

import java.util.List;

import br.com.martinho.githubrepositories.data.localDatabase.GitHubCache;
import br.com.martinho.githubrepositories.data.repository.GitHubListPullRequestsRepository;
import br.com.martinho.githubrepositories.data.repository.base.BaseRepositoryImpl;
import br.com.martinho.githubrepositories.data.service.GitHubService;
import br.com.martinho.githubrepositories.domain.PullRequest;
import br.com.martinho.githubrepositories.util.Constants;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Thiago on 07/10/2016.
 */

public class GitHubListPullRequestsRepositoryImpl extends BaseRepositoryImpl implements GitHubListPullRequestsRepository {

    private GitHubService service;
    private GitHubCache cache;

    public GitHubListPullRequestsRepositoryImpl(GitHubService service, GitHubCache cache) {
        this.service = service;
        this.cache = cache;
    }

    @Override
    public Observable<List<PullRequest>> getPullRequests(String owner, String repository) {
        if (cache.hasCache(Constants.PULL_REQUEST_CACHE_KEY + owner + repository)) {
            return cache.restoreCache(Constants.PULL_REQUEST_CACHE_KEY + owner + repository).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        } else {
            return service.getPullRequests(owner, repository)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(listResponse -> this.cache.addCache(Constants.PULL_REQUEST_CACHE_KEY + owner + repository, listResponse));
        }
    }
}
