package br.com.martinho.githubrepositories.data.localDatabase.impl;

import br.com.martinho.githubrepositories.data.localDatabase.GitHubCache;
import io.paperdb.Paper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Thiago on 06/10/2016.
 */

public class GitHubCacheImpl<T> implements GitHubCache<T> {

    @Override
    public void addCache(String key, T value) {
        Observable.create((Observable.OnSubscribe<Void>) subscriber -> {
            if (!subscriber.isUnsubscribed()) {
                try {
                    Paper.book().write(key, value);
                    subscriber.onNext(null);
                } catch (Exception ignored) {
                }
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    @Override
    public boolean hasCache(String key) {
        T cache = Paper.book().read(key);
        return cache != null;
    }

    @Override
    public Observable<T> restoreCache(String key) {
        return Observable.create(subscriber -> {
            T cache = Paper.book().read(key);
            subscriber.onNext(cache);
        });
    }
}
