package br.com.martinho.githubrepositories;

import android.app.Application;

import br.com.martinho.githubrepositories.di.component.DaggerMainComponent;
import br.com.martinho.githubrepositories.di.component.MainComponent;
import io.paperdb.Paper;


/**
 * Created by Thiago on 21/09/2016.
 */

public class App extends Application {

    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Paper.init(this);

        dependencyInjection();
    }

    private void dependencyInjection() {
        mainComponent = DaggerMainComponent.create();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
