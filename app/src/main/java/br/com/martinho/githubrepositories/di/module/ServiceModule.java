package br.com.martinho.githubrepositories.di.module;

import javax.inject.Singleton;

import br.com.martinho.githubrepositories.data.service.GitHubService;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Thiago on 21/09/2016.
 */

@Module
public class ServiceModule {

    @Provides
    @Singleton
    GitHubService provideAPI(Retrofit retrofit) {
        return retrofit.create(GitHubService.class);
    }
}
