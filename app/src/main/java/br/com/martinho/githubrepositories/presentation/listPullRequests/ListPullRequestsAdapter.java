package br.com.martinho.githubrepositories.presentation.listPullRequests;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.domain.PullRequest;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Thiago on 07/10/2016.
 */

public class ListPullRequestsAdapter extends RecyclerView.Adapter<ListPullRequestsAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;

    private ListPullRequestsContract.View view;
    private Context context;

    ListPullRequestsAdapter(ListPullRequestsContract.View view, List<PullRequest> pullRequests) {
        this.view = view;
        this.pullRequests = pullRequests;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.adapter_pull_requests, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = this.pullRequests.get(position);

        if (!TextUtils.isEmpty(pullRequest.getUser().getAvatarUrl())) {
            Picasso.with(this.context).load(pullRequest.getUser().getAvatarUrl()).into(holder.image);
        }

        Format formatter = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = formatter.format(pullRequest.getCreatedAt());

        String text = pullRequest.getTitle() + " - " + formattedDate;
        holder.title.setText(text);
        holder.body.setText(pullRequest.getBody());
        holder.username.setText(pullRequest.getUser().getLogin());

        holder.view.setOnClickListener(v -> this.view.onItemClick(pullRequest.getHtmlUrl()));
    }

    @Override
    public int getItemCount() {
        return this.pullRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.adapter_pull_requests_title)
        TextView title;

        @BindView(R.id.adapter_pull_requests_body)
        TextView body;

        @BindView(R.id.adapter_pull_requests_image)
        ImageView image;

        @BindView(R.id.adapter_pull_requests_username)
        TextView username;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }
}
