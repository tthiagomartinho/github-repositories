package br.com.martinho.githubrepositories.util;

/**
 * Created by Thiago on 06/10/2016.
 */

public interface Constants {

    //API CONSTANTS
    String LANGUAGE = "language:Java";
    String SORT_STAR = "stars";
    String LINK_HEADER = "Link";
    String REL_LINK_HEADER = "rel";
    String LAST = "last";
    String PAGE_EQUALS = "page=";
    String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";


    //DB CONSTANTS
    String REPOSITORY_CACHE_KEY = "REPOSITORY_CACHE";
    String PULL_REQUEST_CACHE_KEY = "PULL_REQUEST_CACHE";

    //BUNDLES
    String BUNDLE_REPOSITORY_OWNER = "REPOSITORY_OWNER";
    String BUNDLE_REPOSITORY_NAME = "REPOSITORY_NAME";

}
