package br.com.martinho.githubrepositories.presentation.listPullRequests;

import br.com.martinho.githubrepositories.data.repository.GitHubListPullRequestsRepository;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Thiago on 07/10/2016.
 */

public class ListPullRequestsActivityPresenter implements ListPullRequestsContract.Presenter {

    private GitHubListPullRequestsRepository repository;

    private CompositeSubscription compositeSubscription;
    private ListPullRequestsContract.View view;

    public ListPullRequestsActivityPresenter(GitHubListPullRequestsRepository repository) {
        this.repository = repository;
        this.compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void setView(ListPullRequestsContract.View view) {
        this.view = view;
    }

    @Override
    public void loadData(String owner, String repository) {
        this.compositeSubscription.add(this.repository.getPullRequests(owner, repository).subscribe(repositories ->
                        this.view.addData(repositories),
                throwable -> this.view.showErrorLayout()));
    }

    @Override
    public void onDestroy() {
        this.compositeSubscription.unsubscribe();
    }
}
