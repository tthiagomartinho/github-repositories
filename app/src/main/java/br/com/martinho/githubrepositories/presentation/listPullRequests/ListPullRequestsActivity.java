package br.com.martinho.githubrepositories.presentation.listPullRequests;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.domain.PullRequest;
import br.com.martinho.githubrepositories.presentation.base.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import static br.com.martinho.githubrepositories.util.Constants.BUNDLE_REPOSITORY_NAME;
import static br.com.martinho.githubrepositories.util.Constants.BUNDLE_REPOSITORY_OWNER;

/**
 * Created by Thiago on 07/10/2016.
 */

public class ListPullRequestsActivity extends BaseActivity implements ListPullRequestsContract.View {

    @Inject
    ListPullRequestsContract.Presenter presenter;

    @BindView(R.id.activity_list_pull_requests_list)
    RecyclerView pullRequestsList;

    @BindView(R.id.activity_list_pull_requests_progress_bar)
    ProgressBar progressBar;

    private String repositoryOwner;
    private String repositoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pull_requests);

        ButterKnife.bind(this);
        getUiComponent().inject(this);
        this.presenter.setView(this);

        retrieveDataFromIntent();

        if (getSupportActionBar() != null) {
            String title = getString(R.string.pull_requests) + " - " + this.repositoryName;
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initRecyclerView();
        this.presenter.loadData(this.repositoryOwner, this.repositoryName);
    }

    private void retrieveDataFromIntent() {
        if (getIntent().getExtras() != null) {
            this.repositoryOwner = getIntent().getStringExtra(BUNDLE_REPOSITORY_OWNER);
            this.repositoryName = getIntent().getStringExtra(BUNDLE_REPOSITORY_NAME);
        }
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        this.pullRequestsList.setLayoutManager(layoutManager);
    }

    @Override
    public void showLoadingLayout() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void addData(List<PullRequest> repositories) {
        ListPullRequestsAdapter adapter = new ListPullRequestsAdapter(this, repositories);
        this.pullRequestsList.setAdapter(adapter);
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorLayout() {
        DialogInterface.OnClickListener onClickTryAgain = (dialog, which) -> {
            ListPullRequestsActivity.this.presenter.loadData(this.repositoryOwner, this.repositoryName);
        };
        super.displayErrorMessageTryAgain(R.string.error_loading_pull_requests, onClickTryAgain);
    }

    @Override
    public void onItemClick(String url) {
        if (!TextUtils.isEmpty(url)) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}
