package br.com.martinho.githubrepositories.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Thiago on 06/10/2016.
 */

public class RepositoryResponse implements Serializable {

    private List<Repository> items;

    public List<Repository> getItems() {
        return items;
    }
}
