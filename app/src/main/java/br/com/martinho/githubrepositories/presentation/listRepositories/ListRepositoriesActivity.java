package br.com.martinho.githubrepositories.presentation.listRepositories;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.presentation.base.BaseActivity;
import br.com.martinho.githubrepositories.presentation.listPullRequests.ListPullRequestsActivity;
import br.com.martinho.githubrepositories.util.listeners.EndlessRecyclerViewScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;

import static br.com.martinho.githubrepositories.util.Constants.BUNDLE_REPOSITORY_NAME;
import static br.com.martinho.githubrepositories.util.Constants.BUNDLE_REPOSITORY_OWNER;

public class ListRepositoriesActivity extends BaseActivity implements ListRepositoriesContract.View {

    @Inject
    ListRepositoriesContract.Presenter presenter;

    @BindView(R.id.activity_list_repositories_list)
    RecyclerView repositoriesList;

    @BindView(R.id.activity_list_repositories_progress_bar)
    ProgressBar progressBar;

    private ListRepositoriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_repositories);

        ButterKnife.bind(this);
        getUiComponent().inject(this);
        this.presenter.setView(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.app_name);
        }

        initRecyclerView();
        this.presenter.loadData(1);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        this.adapter = new ListRepositoriesAdapter(this, new ArrayList<>());
        this.repositoriesList.setAdapter(this.adapter);
        this.repositoriesList.setLayoutManager(layoutManager);
        this.repositoriesList.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (ListRepositoriesActivity.this.presenter.canRequestMoreData(page + 1)) {
                    ListRepositoriesActivity.this.presenter.loadData(page + 1);
                }
            }
        });
    }

    @Override
    public void showLoadingLayout() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void addData(List<Repository> repositories) {
        this.adapter.addMoreItems(repositories);
        this.adapter.notifyDataSetChanged();
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorLayout(int page) {
        if (isConnectedToInternet() || this.adapter.getItemCount() == 0) {
            DialogInterface.OnClickListener onClickTryAgain = (dialog, which) -> {
                ListRepositoriesActivity.this.presenter.loadData(page);
            };
            super.displayErrorMessageTryAgain(R.string.error_loading_repositories, onClickTryAgain);
        }
        this.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClickItem(String ownerName, String repositoryName) {
        Intent intent = new Intent(this, ListPullRequestsActivity.class);
        intent.putExtra(BUNDLE_REPOSITORY_OWNER, ownerName);
        intent.putExtra(BUNDLE_REPOSITORY_NAME, repositoryName);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }
}
