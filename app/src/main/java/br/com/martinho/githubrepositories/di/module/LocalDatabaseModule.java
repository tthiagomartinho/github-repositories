package br.com.martinho.githubrepositories.di.module;

import javax.inject.Singleton;

import br.com.martinho.githubrepositories.data.localDatabase.GitHubCache;
import br.com.martinho.githubrepositories.data.localDatabase.impl.GitHubCacheImpl;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Thiago on 06/10/2016.
 */

@Module
public class LocalDatabaseModule {

    @Provides
    @Singleton
    GitHubCache provideGitHubRepositoryCache() {
        return new GitHubCacheImpl();
    }
}
