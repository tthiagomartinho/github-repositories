package br.com.martinho.githubrepositories.presentation.listRepositories;

import br.com.martinho.githubrepositories.data.repository.GitHubRepositoriesRepository;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Thiago on 06/10/2016.
 */

public class ListRepositoriesActivityPresenter implements ListRepositoriesContract.Presenter {


    private ListRepositoriesContract.View view;
    private CompositeSubscription compositeSubscription;
    private GitHubRepositoriesRepository repository;


    public ListRepositoriesActivityPresenter(GitHubRepositoriesRepository repository) {
        this.repository = repository;
        this.compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void setView(ListRepositoriesContract.View view) {
        this.view = view;
    }

    @Override
    public void loadData(int page) {
        this.view.showLoadingLayout();
        this.compositeSubscription.add(
                this.repository.getRepositories(page).subscribe(repositories ->
                                this.view.addData(repositories),
                        throwable -> this.view.showErrorLayout(page)));
    }

    @Override
    public boolean canRequestMoreData(int page) {
        return this.repository.canRequestMoreData(page);
    }

    @Override
    public void onDestroy() {
        if (this.compositeSubscription != null) {
            this.compositeSubscription.unsubscribe();
        }
        this.view = null;
    }
}
