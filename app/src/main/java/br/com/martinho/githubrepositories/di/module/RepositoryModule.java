package br.com.martinho.githubrepositories.di.module;

import br.com.martinho.githubrepositories.data.localDatabase.GitHubCache;
import br.com.martinho.githubrepositories.data.repository.GitHubListPullRequestsRepository;
import br.com.martinho.githubrepositories.data.repository.GitHubRepositoriesRepository;
import br.com.martinho.githubrepositories.data.repository.impl.GitHubListPullRequestsRepositoryImpl;
import br.com.martinho.githubrepositories.data.repository.impl.GitHubRepositoriesRepositoryImpl;
import br.com.martinho.githubrepositories.data.service.GitHubService;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Thiago on 21/09/2016.
 */

@Module
public class RepositoryModule {
    @Provides
    GitHubRepositoriesRepository provideGitHubRepositoriesRepository(GitHubService service, GitHubCache cache) {
        return new GitHubRepositoriesRepositoryImpl(service, cache);
    }

    @Provides
    GitHubListPullRequestsRepository provideGitHubPullRequestsRepository(GitHubService service, GitHubCache cache) {
        return new GitHubListPullRequestsRepositoryImpl(service, cache);
    }
}
