package br.com.martinho.githubrepositories.data.repository.impl;

import java.util.List;

import br.com.martinho.githubrepositories.data.localDatabase.GitHubCache;
import br.com.martinho.githubrepositories.data.repository.GitHubRepositoriesRepository;
import br.com.martinho.githubrepositories.data.repository.base.BaseRepositoryImpl;
import br.com.martinho.githubrepositories.data.service.GitHubService;
import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.domain.RepositoryResponse;
import br.com.martinho.githubrepositories.util.Constants;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Thiago on 06/10/2016.
 */

public class GitHubRepositoriesRepositoryImpl extends BaseRepositoryImpl implements GitHubRepositoriesRepository {
    private GitHubService service;
    private GitHubCache cache;

    public GitHubRepositoriesRepositoryImpl(GitHubService service, GitHubCache cache) {
        this.service = service;
        this.cache = cache;
    }

    @Override
    public Observable<List<Repository>> getRepositories(int page) {
        if (cache.hasCache(Constants.REPOSITORY_CACHE_KEY + page)) {
            return cache.restoreCache(Constants.REPOSITORY_CACHE_KEY + page).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        } else {
            return service.getRepositories(Constants.LANGUAGE, Constants.SORT_STAR, page)
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).concatMap(listResponse -> parseHeaderCache(listResponse, page));
        }
    }

    @Override
    public Observable<List<Repository>> parseHeaderCache(Response<RepositoryResponse> listResponse, int page) {
        if (this.maxPages <= 0) {
            this.maxPages = parseHeader(listResponse.headers());
        }

        List<Repository> repositories = listResponse.body().getItems();
        this.cache.addCache(Constants.REPOSITORY_CACHE_KEY + page, repositories);

        return Observable.just(repositories);
    }
}
