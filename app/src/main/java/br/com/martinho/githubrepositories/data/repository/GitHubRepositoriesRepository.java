package br.com.martinho.githubrepositories.data.repository;

import java.util.List;

import br.com.martinho.githubrepositories.data.repository.base.BaseRepository;
import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.domain.RepositoryResponse;
import okhttp3.Headers;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by Thiago on 06/10/2016.
 */

public interface GitHubRepositoriesRepository extends BaseRepository {

    Observable<List<Repository>> getRepositories(int page);

    int parseHeader(Headers headers);

    Observable<List<Repository>> parseHeaderCache(Response<RepositoryResponse> listResponse, int page);
}
