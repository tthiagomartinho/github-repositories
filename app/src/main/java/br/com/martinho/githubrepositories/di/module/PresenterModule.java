package br.com.martinho.githubrepositories.di.module;

import br.com.martinho.githubrepositories.data.repository.GitHubListPullRequestsRepository;
import br.com.martinho.githubrepositories.data.repository.GitHubRepositoriesRepository;
import br.com.martinho.githubrepositories.di.scope.PerActivity;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesActivityPresenter;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesContract;
import br.com.martinho.githubrepositories.presentation.listPullRequests.ListPullRequestsActivityPresenter;
import br.com.martinho.githubrepositories.presentation.listPullRequests.ListPullRequestsContract;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Thiago on 21/09/2016.
 */

@Module
public class PresenterModule {

    @PerActivity
    @Provides
    ListRepositoriesContract.Presenter providesListRepositoriesActivityPresenter(GitHubRepositoriesRepository repository) {
        return new ListRepositoriesActivityPresenter(repository);
    }

    @PerActivity
    @Provides
    ListPullRequestsContract.Presenter providesPullRequestsActivityPresenter(GitHubListPullRequestsRepository repository) {
        return new ListPullRequestsActivityPresenter(repository);
    }
}
