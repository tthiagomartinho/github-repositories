package br.com.martinho.githubrepositories.presentation.listRepositories;

import java.util.List;

import br.com.martinho.githubrepositories.domain.Repository;
import br.com.martinho.githubrepositories.presentation.base.BasePresenterContract;
import br.com.martinho.githubrepositories.presentation.base.BaseViewContract;

/**
 * Created by Thiago on 06/10/2016.
 */

public interface ListRepositoriesContract {

    interface View extends BaseViewContract {

        void addData(List<Repository> repositories);

        void showErrorLayout(int page);

        void onClickItem(String ownerName, String repositoryName);
    }

    interface Presenter extends BasePresenterContract {

        void setView(ListRepositoriesContract.View view);

        void loadData(int page);

        boolean canRequestMoreData(int page);
    }
}
