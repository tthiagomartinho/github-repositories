package br.com.martinho.githubrepositories.presentation.base;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import br.com.martinho.githubrepositories.App;
import br.com.martinho.githubrepositories.R;
import br.com.martinho.githubrepositories.di.component.UiComponent;


/**
 * Created by Thiago on 21/09/2016.
 */

public class BaseActivity extends AppCompatActivity {

    private UiComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUiComponent();
    }

    private void initUiComponent() {
        component = ((App) getApplication()).getMainComponent().uiComponent();
    }

    protected UiComponent getUiComponent() {
        return component;
    }

    protected boolean isConnectedToInternet() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    protected void displayErrorMessage(@StringRes int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> dialog.dismiss());
        builder.create().show();
    }

    protected void displayErrorMessageTryAgain(@StringRes int message, DialogInterface.OnClickListener tryAgainAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.try_again, tryAgainAction)
                .setNegativeButton(R.string.cancel, ((dialog, which) -> this.finish()));
        builder.create().show();
    }
}
