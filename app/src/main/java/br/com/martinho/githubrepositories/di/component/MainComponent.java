package br.com.martinho.githubrepositories.di.component;

import javax.inject.Singleton;

import br.com.martinho.githubrepositories.di.module.ApplicationModule;
import br.com.martinho.githubrepositories.di.module.LocalDatabaseModule;
import br.com.martinho.githubrepositories.di.module.NetworkModule;
import br.com.martinho.githubrepositories.di.module.RepositoryModule;
import br.com.martinho.githubrepositories.di.module.ServiceModule;
import dagger.Component;

/**
 * Created by Thiago on 21/09/2016.
 */


@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        RepositoryModule.class,
        ServiceModule.class,
        LocalDatabaseModule.class
})
public interface MainComponent {

    UiComponent uiComponent();
}
