package br.com.martinho.githubrepositories.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Thiago on 07/10/2016.
 */

public class PullRequest implements Serializable {

    private String title;

    private User user;

    private String htmlUrl;

    private String body;

    private Date createdAt;

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public User getUser() {
        return user;
    }
}
