package br.com.martinho.githubrepositories.di.component;

import br.com.martinho.githubrepositories.di.module.PresenterModule;
import br.com.martinho.githubrepositories.di.scope.PerActivity;
import br.com.martinho.githubrepositories.presentation.listPullRequests.ListPullRequestsActivity;
import br.com.martinho.githubrepositories.presentation.listRepositories.ListRepositoriesActivity;
import dagger.Subcomponent;

/**
 * Created by Thiago on 21/09/2016.
 */

@PerActivity
@Subcomponent(modules = {PresenterModule.class})
public interface UiComponent {
    void inject(ListRepositoriesActivity listRepositoriesActivity);

    void inject(ListPullRequestsActivity listPullRequestsActivity);

}
