package br.com.martinho.githubrepositories.domain;

import java.io.Serializable;

/**
 * Created by Thiago on 06/10/2016.
 */

public class User implements Serializable {
    private String avatarUrl;

    private String login;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLogin() {
        return login;
    }
}
