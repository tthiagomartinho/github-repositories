package br.com.martinho.githubrepositories.data.localDatabase;

import rx.Observable;

/**
 * Created by Thiago on 06/10/2016.
 */

public interface GitHubCache<T> {

    void addCache(String key, T value);

    boolean hasCache(String key);

    Observable<T> restoreCache(String key);
}
